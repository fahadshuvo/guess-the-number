# Guess The Number
A fun guess the number game using Tkinter in Python

## About:
***Guess The Number*** <br>
From 1 to 100, the computer randomly selects a number.
The user must also make a numerical guess.
The Hint is updated so that the user can quickly guess the number each time they guess it incorrectly.
The computer displays an error if the user enters the incorrect number. 

***Tkinter Python*** <br>
Tkinter is a GUI framework for Python that allows for buttons, graphics and alot of other things to be done in a window
<br>

<p align="center">Calculator<br>
  <img src="Images/y.png" width="450" height="300"><p/>

## Demo:

<p align="center" width="100%">
    <img width="45%" height="250" src="Images/z.png">
    <img width="45%" height="250" src="Images/zz.png">
    <img width="45%" height="250" src="Images/xz.png">
</p>
